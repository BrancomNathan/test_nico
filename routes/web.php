<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/upload', 'UploadController@index')->name('uploadIndex');
Route::post('/upload', 'UploadController@uploadFile')->name('uploadIndex');

Route::get('/getfile', 'FileController@index');
Route::post('/getfile', 'FileController@getFile');
