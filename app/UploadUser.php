<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadUser extends Model
{
    protected $fillable = [
        'name', 'email', 'user_id', 'is_registered_user',
    ];
}
