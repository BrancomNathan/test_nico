<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upload;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function index()
    {
        return view('filecode');
    }

    public function getFile(Request $request)
    {
        $upload = Upload::where('code', $request->input("fileCode"))->first();
        $filePath = Storage::url($upload->file_path);
        return view('viewFile', ['filePath' => $filePath]);
    }

}
