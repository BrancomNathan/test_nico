<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Upload;
use App\UploadUser;
use Auth;
use Illuminate\Support\Str;
use Input;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Support\Facades\Mail;
use App\Mail\FileMail;


class UploadController extends Controller
{
    public function index()
    {
        return view('upload');
    }

    public function uploadFile(Request $request)
    {

        $userId = null;
        if(Auth::check())
        {
            $userId = Auth::user()->id;
        }

        $uploadUser = $this->GetUploadUser($request->input('senderEmail'), $request->input('name'), $userId);
        $receiverUploadUser = $this->GetUploadUser($request->input('receiverEmail'), $request->input('receiverName'));
        if($uploadUser != null && $receiverUploadUser != null)
        {
            $filePath = $request->file->store("public/images");

            $upload = new Upload();
            $upload->sender_id = $uploadUser->id;
            $upload->receiver_id = $receiverUploadUser->id;
            $upload->file_name = $filePath;
            $upload->file_path = $filePath;
            $upload->save();
            $upload->code = Str::random(4) . $upload->id . rand(1, 1000);
            $upload->save();

            $senderName = null;
            if($uploadUser->is_registered_user)
            {
                $user = User::find($uploadUser->user_id);
                $senderName = $user->name;
            }
            else
            {
                $senderName = $uploadUser->name;
            }
            Mail::to($receiverUploadUser->email)->send(new FileMail($senderName, $upload->code));
            $upload->is_sent = true;
            $upload->sent_date = date("Y-m-d H:i:s");
            $upload->save();

        }



    }


    private function GetUploadUser(String $email, String $name, int $userId = null)
    {
        try
        {
            $uploadUser = null;
            if($userId != null)
            {
                $uploadUser = UploadUser::where("user_id", $userId)->first();
                if($uploadUser == null)
                {
                    $uploadUser = new UploadUser();
                    $uploadUser->user_id = $userId;
                    $uploadUser->is_registered_user = true;
                    $uploadUser->save();
                }
            }
            else
            {
                $uploadUser = UploadUser::where("email", $email)->first();
                if($uploadUser == null)
                {
                    $uploadUser = new UploadUser();
                    $uploadUser->name = $name;
                    $uploadUser->email = $email;
                    $uploadUser->is_registered_user = false;
                    $uploadUser->save();

                }
            }

            return $uploadUser;
        }
        catch (Exception $e)
        {
            return null;
        }

    }


}
