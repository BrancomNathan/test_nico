<div>
    {{ $name }} has sent you a File!<br>
    {{ $code }} <br>
    Please fill in your code <a href="{{ URL::to('/getfile') }}">here</a> to download your file
    <br>
    {{ URL::to('/getfile') }}
</div>
