@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Upload Module</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            {!! Form::open(['files' => true]) !!}

                                <div class="form-group">
                                    {!! Form::label('name', 'Name:') !!}
                                    <br>
                                    @if(Auth::check())
                                        {{ Auth::user()->name }}
                                        {!! Form::text('name', Auth::user()->name, ['class' => 'd-none']) !!}

                                    @else
                                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    @endif
                                </div>

                                <div class="form-group">
                                    {!! Form::label('senderEmail', 'Your e-mail:') !!}
                                    <br>
                                    @if(Auth::check())

                                        {{ Auth::user()->email }}
                                        {!! Form::email('senderEmail', Auth::user()->email, ['class' => 'd-none']) !!}

                                    @else
                                        {!! Form::email('senderEmail', null, ['class' => 'form-control']) !!}
                                    @endif
                                </div>

                                <div class="form-group">
                                    {!! Form::label('receiverEmail', 'Receiver E-mail') !!}
                                    {!! Form::email('receiverEmail', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('receiverName', 'Name Receiver:') !!}
                                    <br>
                                    {!! Form::text('receiverName', null, ['class' => 'form-control']) !!}
                                </div>


                            <div class="form-group">
                                    {!! Form::label('file', 'Bestand') !!}
                                    {!! Form::file('file') !!}
                                </div>

                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

                            {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
