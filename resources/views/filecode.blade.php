@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            {!! Form::open() !!}



                            <div class="form-group">
                                {!! Form::label('fileCode', 'File Code:') !!}
                                <br>
                                {!! Form::text('fileCode', null, ['class' => 'form-control']) !!}
                            </div>


                            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
